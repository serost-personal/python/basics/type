# typing is builtin i believe, so MAGIC
from typing import List, Dict, Set, Union, Optional, Any, Sequence, Callable, TypeVar

# or import Self from typing if using python 3.11+
Self = Any

ListAllias = List
ListInts = List[int]
DictStrInt = Dict[str, int]
SetInts = Set[int]

ListIntsOrStrs = List[int | str]


class Test:
    # def ret_self() -> Test does not work, see
    # https://peps.python.org/pep-0673/
    # but requires python 3.11+
    @staticmethod
    def ret_self() -> Self:
        pass


# -> None is used for mypy, otherwise you can run
# mypy ./0_type_hints.py --check-untyped-defs
def main() -> None:
    # type hints are ignored by interpreter, and used for more
    # readable code / editor hints
    # a: int = "this is string"  # mypy will throw an error, but for python it is ok
    # print(a)  # yep, this is legal (in python, but not in mypy)
    l: List[int] = list()
    d: Dict[str, int] = {"key": 5555}

    li: ListInts = [1, 2, 3, 4]
    si: SetInts = {1, 2, 3, 3, 4}
    lis: ListIntsOrStrs = [1, "two", 3, "four"]

    x: tuple[int, int, int] = (1, 2, 3)

    print(isinstance(1, int | str))  # True
    # print(isinstance(1, Union[int, str]))  # True, but mypy throws incompatible

    print(type(int | str))  # <class 'types.UnionType'> use this, since newer
    print(type(Union[int, str]))  # <class 'typing._UnionGenericAlias'>
    # same as Union[int, type(None)]
    # or Union[int, NoneType]
    print(type(Optional[int]))  # <class 'typing._UnionGenericAlias'>

    # see https://peps.python.org/pep-0604/
    print((int | str) == (str | int))  # True
    print((int | str) == Union[str, int])  # True

    def func(something: Any, opt: Optional[int] = None) -> None:
        pass

    func(555)

    def seq(seq: Sequence[int]) -> None:
        pass

    seq([1, 2, 3])
    # seq("12345") # type "str"; expected Sequence[int]

    def seq_str(seq: Sequence[str]) -> str:
        return ""

    seq_str("12345")  # ok
    seq_str(["1234", "5678"])  # ok
    # seq_str([1234, 5678])  # type "int"; expected "str"

    def simple_func(int, float) -> str:
        return ""

    simple_fobj: Callable[[int, float], str] = simple_func

    # Callable[[arg1_type, arg2_type], return_type]
    def func2(v: str, v2: Optional[int] = None) -> Callable[[Sequence[str]], None]:
        return func

    obscure_fobj: Callable[[str, Optional[int]], Callable[[Sequence[str]], None]] = (
        func2
    )

    # https://docs.python.org/3/reference/compound_stmts.html#type-parameter-lists
    # since python 3.12+
    # def first[T](l: Sequence[T]) -> T:
    #     return l[0]

    T = TypeVar("T")

    # or T | type(None)
    def first(l: list[T]) -> Optional[T]:
        if len(l) == 0:
            return None
        return l[0]

    print(first([1, 2, 3]))
    print(first([1.0, 2.0, 3.0]))
    print(first([]))


if __name__ == "__main__":
    main()
